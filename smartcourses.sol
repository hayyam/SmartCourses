pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

		
contract SmartCourses{
    
    struct Lecture{
        string name;
        uint16 maxStudent;
        uint8 dailyCourseFee;
        uint8 courseDays;
        uint totalFee;
        address lecAddress;
        uint lectureWallet;
        bool isEnded;
    }

    struct Institution{
        address InstAddress;
    	string name;
    }

    struct Lecturer{
        address lecAddress;
        string name;
        bytes32 lecturerKey;
    }
    
    struct Student{
        string name;
        address stuAddress;
        bytes32 hashOfKey;
        bool isValid;
        uint id;
    }
    

    Student[] students;
    uint16 studentAmount = 0; //Total number of students
    uint8 lectureAmount = 0;
    Lecturer lecturer;  //Create a lecturer instance from Lecturer struct
    Lecture[] lectures;
    address vakif;
    
    function setLecturer() public{
        
        lecturer.lecAddress = 0x123;
        lecturer.name = "Engur";
        lecturer.lecturerKey = keccak256("is_satoshi");
    
    }

    bytes32[][] dailyKeys;
    /* Daily keys are determined by lecturer for each day of course. A student is validated for course when its
all daily keys are determined. Each day of course, lecturer gives the code of this day to the student. Student enters this
key to attendance function and proves the attendance. We assume a ideal lecturer. */


    mapping(string=>Student) studentMap;
    mapping(string=>Lecture) lectureMap;


    function createLecture(string _lecturerKey, string _name, uint16 _maxStu, uint8 _fee, uint8 _days) public{
	    require(
	        lecturer.lecturerKey== keccak256(abi.encodePacked(_lecturerKey)),
	        "You do not have permission to create course."
	    );
        
        lectures.push(Lecture({
            name : _name,
            maxStudent : _maxStu,
            dailyCourseFee : _fee,
            courseDays : _days,
            lecAddress : lecturer.lecAddress,
            isEnded : false,
            totalFee : _days*_fee,
            lectureWallet : 0
        }));
        lectureMap[_name]=lectures[lectureAmount];
        lectureAmount++;
    }
    
    
    function LectureRegistration(string _name, string _key, string _lectureName) payable public {
        require(
            msg.value >= lectureMap[_lectureName].totalFee, 
            "Amount is not feasible."
        );
        students.push(Student({
            name : _name,
            hashOfKey : keccak256(abi.encodePacked(_key)),
            stuAddress : msg.sender,
            isValid : false,
            id : studentAmount
        }));
        studentMap[_name]=students[studentAmount];
        studentAmount++;
        lectureMap[_lectureName].lectureWallet+= msg.value;
    }



    function validateStudent (string _studentName, string[] _dailyKeyArray) public{
        
        require(
            students[i].isValid==false, "Ogrenci onceden kayit edilmis."
        );
        
        for (uint i=0; i<_dailyKeyArray.length;i++){
            dailyKeys[studentMap[_studentName].id][i]=keccak256(abi.encodePacked(_dailyKeyArray[i]));
        }
        
        students[i].isValid = true;
    }
    
    function attendance (string _name, string _studentKey, string _dailyKey, uint _day, string _lecName) public{
        require( 
            studentMap[_name].hashOfKey == sha256(abi.encodePacked(_studentKey)), 
            "Authentication failured." 
            );
        
        require(
            studentMap[_name].isValid == true,
            "Student has not validated yet."
        );
        
        require(
            dailyKeys[studentMap[_name].id][_day] == sha256(abi.encodePacked(_dailyKey)),
            "Authentication failured." 
        );
        
        require(
            lectureMap[_lecName].isEnded==false, 
            "Course has already ended."
        );
        
        dailyKeys[studentMap[_name].id][_day] == 0;
        msg.sender.transfer( lectureMap[_lecName].dailyCourseFee /2 );
        lectureMap[_lecName].lecAddress.transfer( lectureMap[_lecName].dailyCourseFee/2 );
        lectureMap[_lecName].lectureWallet-=lectureMap[_lecName].dailyCourseFee;
    }
    
     function theEnd (uint _lecturerKey, string _lecName) public{
        require(
            lecturer.lecturerKey==keccak256(abi.encodePacked(_lecturerKey)), 
            "Authentication failured."
        );
        
        vakif.transfer(lectureMap[_lecName].lectureWallet);
        lectureMap[_lecName].lectureWallet=0;
        lectureMap[_lecName].isEnded = true;

    }
     
}
